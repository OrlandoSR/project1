package edu.uprm.ece.icom4035.polynomial;

public class TermImp implements Term {
	
	private double Coefficient;
	
	private int Exponent;
	
	public TermImp(double c, int e) {
		Coefficient = c;
		Exponent = e;
	}
	
	public TermImp() {
	}

	@Override
	public double getCoefficient() {
		return Coefficient;
	}

	@Override
	public int getExponent() {
		return Exponent;
	}

	@Override
	public double evaluate(double x) {
		return (Math.pow(x, getExponent()))*getCoefficient();
	}
	
	public void setCoefficient(double c) {
		this.Coefficient = c;
	}
	
	public void setExponent(int e) {
		this.Exponent = e;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(((TermImp) obj).getCoefficient() == this.getCoefficient() && ((TermImp) obj).getExponent() == this.getExponent()) {
			return true;
		}
		return false;
	}

}
