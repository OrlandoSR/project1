package edu.uprm.ece.icom4035.polynomial;

import java.util.Iterator;

import edu.uprm.ece.icom4035.list.List;
import edu.uprm.ece.icom4035.list.ListIterator1;

public class PolynomialImp implements Polynomial {

	private static char separatorChar = '+';
	private int NumberOfTerms;
	public List<Term> PolynomialList;

	public PolynomialImp(String string) {
		PolynomialList = (List<Term>) TermListFactory.newListFactory().newInstance();
		NumberOfTerms = numTerms(string);
		Term[] termsOfPoly = Terms(TermSubstrings(string, NumberOfTerms), NumberOfTerms);
		for (int i = 0; i < termsOfPoly.length; i++) {
			PolynomialList.add(termsOfPoly[i]);
		}
	}

	public PolynomialImp() {
		PolynomialList = (List<Term>) TermListFactory.newListFactory().newInstance();
	}

	public PolynomialImp(List<Term> list) {
		PolynomialList = list;
	}

	@Override
	public Iterator<Term> iterator() {
		ListIterator1<Term> iter = new ListIterator1<Term>(this.PolynomialList);
		return iter;
	}

	@Override
	public Polynomial add(Polynomial P2) {
		Polynomial P3 = new PolynomialImp();
		for(Term t: this.getPolyList()) {
			this.add(t, P2, P3);
		}
		for(Term t: ((PolynomialImp) P2).getPolyList()) {
			this.add(t, this, P3);
		}
		return ((PolynomialImp) P3).sortbyDegree();
	}

	private Polynomial add(Term t, Polynomial p, Polynomial r) {
		boolean wasAdded = false;
		PolynomialImp Result = (PolynomialImp) r;
		for(int i = 0; i < ((PolynomialImp)p).getPolyList().size(); i++) {
			Term current = ((PolynomialImp)p).getPolyList().get(i);
			if(t.getExponent() == current.getExponent()) { 
				Term tta = new TermImp(t.getCoefficient() + current.getCoefficient(), t.getExponent());
				if(!Result.getPolyList().contains(tta)){
					Result.getPolyList().add(tta);
				}
				wasAdded = true;
			}
		}
		if(!wasAdded && !Result.getPolyList().contains(t)) {
			Result.getPolyList().add(t);
		}

		return Result;
	}

	@Override
	public Polynomial subtract(Polynomial P2) {
		Polynomial P3 = new PolynomialImp();
		if(this.equals(P2)) return new PolynomialImp("0");
		for(Term t: ((PolynomialImp) P2).getPolyList()) {
			((TermImp) t).setCoefficient(t.getCoefficient()*-1);
		}
		for(Term t: this.getPolyList()) {
			this.add(t, P2, P3);
		}
		for(Term t: ((PolynomialImp) P2).getPolyList()) {
			this.add(t, this, P3);
		}
		return ((PolynomialImp) P3).sortbyDegree();
	}

	@Override
	public Polynomial multiply(Polynomial P2) {
		if(P2.equals(new PolynomialImp("0"))) return new PolynomialImp("0");
		PolynomialImp Result = new PolynomialImp();
		for(int i = 0; i < this.getPolyList().size(); i++) {
			Result = (PolynomialImp) Result.add(termMultiply(this.getPolyList().get(i), P2));
		}
		return Result;
	}

	@Override
	public Polynomial multiply(double c) {
		if(c == 0)  return new PolynomialImp("0");
		for(Term t: PolynomialList) {
			((TermImp)t).setCoefficient(t.getCoefficient()*c);
		}
		return this;
	}

	@Override
	public Polynomial derivative() {
		Polynomial Result = new PolynomialImp();
		for(Term t: this) {
			TermImp newTerm = new TermImp();
			newTerm.setCoefficient(t.getCoefficient()*t.getExponent());
			newTerm.setExponent(t.getExponent() - 1);
			((PolynomialImp)Result).getPolyList().add(newTerm);
		}
		return ((PolynomialImp)Result).validate();
	}

	@Override
	public Polynomial indefiniteIntegral() {
		Polynomial Result = new PolynomialImp();
		for(Term t: this){
			TermImp newTerm = new TermImp();
			newTerm.setCoefficient(t.getCoefficient() / (t.getExponent() + 1));
			newTerm.setExponent(t.getExponent() + 1);
			((PolynomialImp)Result).getPolyList().add(newTerm);
		}
		((PolynomialImp)Result).getPolyList().add(new TermImp(1, 0));
		return ((PolynomialImp)Result).validate();
	}

	@Override
	public double definiteIntegral(double a, double b) {
		return (this.indefiniteIntegral().evaluate(b) - this.indefiniteIntegral().evaluate(a));
	}

	@Override
	public int degree() {
		//pre: Term degrees are in descending order
		return PolynomialList.first().getExponent();
	}

	@Override
	public double evaluate(double x) {
		double result = 0;
		for(Term t: PolynomialList) {
			result = result + t.evaluate(x);
		}
		return result;
	}

	@Override
	public boolean equals(Polynomial P) {
		if(this.getPolyList().size() != ((PolynomialImp)P).getPolyList().size()) return false;
		for(int i = 0; i < this.getPolyList().size(); i++) {
			if(((PolynomialImp)P).getPolyList().get(i).equals(this.getPolyList().get(i))) {
			}else {
				return false;
			}
		}
		return true;
	}


	public String toString(){
		return PolyToString(this);
	}

	public List<Term> getPolyList() {
		return PolynomialList;
	}

	//	These are private instance methods used to change a string given into a list of terms and other operations
	//
	
	//Return the number of terms in a string that represents a polynomial
	private int numTerms(String s) {
		int count = 0;
		if(s.isEmpty()) return count;
		else {
			for(int i = 0; i < s.length(); i++) {
				if(s.charAt(i) == separatorChar ) {
					count++;
				}
			}
			return count + 1;
		}
	}

	//Receives a polynomial as a string and the number of terms in that string and return all the terms in an array of strings
	private String[] TermSubstrings(String s, int NoT) {
		if(NoT == 0) return null;
		int sBindex = -1; //Index of first seprator char in string, begins at negative one as if there was a separator before the beginning of the string
		int sAindex; //Index of the next separtor char in the string
		if(s.contains("" + separatorChar)) { sAindex = s.indexOf(separatorChar); } 	//If there is no separator char then assume last 
		else { sAindex = s.length() + 1; } 											// index of separator char is after last index of string
		String[] substrings = new String[NoT]; //Create an array of strings that contains all terms as substring of main polynomial string given
		for(int i = 0; i < NoT; i++) {
			if(sAindex == s.length() + 1) {
				substrings[i] = s.substring(sBindex + 1);
			}else {
				substrings[i] = s.substring(sBindex + 1, sAindex);
			}
			sBindex = sAindex;
			if(!(sAindex == s.length() + 1)) {
				String s1 = s.substring(sAindex + 1);
				sAindex = (s1.contains("" + separatorChar)) ? s1.indexOf(separatorChar) + (sAindex + 1): s.length() + 1;
			}
		}
		return substrings;
	}
	
	//Receives a string array representing the terms of a polynomial and returns an array of terms
	private TermImp[] Terms(String[] s, int NoT) {
		TermImp[] terms = new TermImp[NoT];
		for(int i = 0; i < s.length; i++) {
			terms[i] = StringToTerm(s[i]);
		}
		return terms;
	}
	
	//Receives a string containing one polynomial term (as a string) and returns it as a Term
	private TermImp StringToTerm(String s) {
		if(s.isEmpty()) return null;
		TermImp term = new TermImp();
		if(s.contains("x")) {
			int IndexOfx = s.indexOf("x");
			//Setting coefficient
			if(IndexOfx == 0) {
				term.setCoefficient(1);
			}else {
				term.setCoefficient(Double.parseDouble(s.substring(0, IndexOfx)));
			}
			//Setting exponent
			if(IndexOfx == (s.length() - 1)) {
				term.setExponent(1);
			}else {
				term.setExponent(Integer.parseInt(s.substring(IndexOfx + 2)));
			}
		}else {
			term.setCoefficient(Double.parseDouble(s));
			term.setExponent(0);
		}

		return term;
	}
	
	//Receives a term and returns a string
	private String TermToString(Term t) {
		if(t.getExponent() == 0) {
			return new String("" + Double.toString(t.getCoefficient()) + "0");
		}else if(t.getExponent() == 1 && t.getCoefficient() != 1) {
			return new String(("" + Double.toString(t.getCoefficient()) + "0" + "x"));
		}else if(t.getCoefficient() == 1 && t.getExponent() == 1){
			return new String("x");
		}else {
			return new String((("" + Double.toString(t.getCoefficient()) + "0") + "x^" + t.getExponent()));
		}
	}
	
	//Takes a polynomial and return the resulting string
	private String PolyToString(PolynomialImp p) {
		String PolyString = "";
		for(Term t: this) {
			PolyString = PolyString + TermToString(t) + "+";
		}
		PolyString = PolyString.substring(0, PolyString.length()-1);
		return PolyString;

	}
	
	//Returns the value of the highest exponent in the polynomial
	private int maxTermExponent() {
		Term maxTerm = this.getPolyList().first();
		for(Term t: this.getPolyList()) {
			if(t.getExponent() > maxTerm.getExponent()) {
				maxTerm = t;
			}
		}
		return maxTerm.getExponent();
	}
	
	//Multiplies a term by a polynomial and return the resulting polynomial
	private static Polynomial termMultiply(Term c, Polynomial p) {
		PolynomialImp Result = new PolynomialImp();

		for(Term t: p) {
			TermImp tta = new TermImp(t.getCoefficient() * c.getCoefficient(), t.getExponent() + c.getExponent());
			Result.getPolyList().add(tta);
		}


		return Result;
	}
	
	//Sorts the polynomial by degree
	private Polynomial sortbyDegree() {
		int mte = maxTermExponent();
		PolynomialImp P1 = new PolynomialImp();
		for(int i = mte; i >= 0; i--) {
			for(Term t: this) {
				if(t.getExponent() == mte) {
					P1.getPolyList().add(t);
				}
			}
			mte--;
		}
		return P1.validate();
	}
	
	//Eliminates all terms with coefficient 0 in the polynomial
	private Polynomial validate() {
		Iterator<Term> iter =  this.iterator();
		Term current = iter.next();

		while(iter.hasNext()) {
			if(current.getCoefficient() == 0) {
				iter.remove();
			}
			current = iter.next();
		}
		if(current.getCoefficient() == 0) {
			iter.remove();
		}
		return this;
	}
}
