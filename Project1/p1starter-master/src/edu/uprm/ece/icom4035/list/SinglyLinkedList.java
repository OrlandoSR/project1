package edu.uprm.ece.icom4035.list;

import java.util.Iterator;

public class SinglyLinkedList<E> implements List<E>{
	
	public Node<E> head;
	public int size;
	
	public SinglyLinkedList() {
		head = new Node<E>();
		size = 0;
	}

	@Override
	public Iterator<E> iterator() {
		ListIterator1<E> iter = new ListIterator1<E>(this);
		return iter;
	}

	@Override
	public void add(E obj) {
		this.add(size, obj);
		
	}

	@Override
	public void add(int index, E obj) {
		Node<E> nuevo = new Node<E>(obj, null);
		if(index == 0) {
			nuevo.setNext(head.getNext());
			head.setNext(nuevo);
		}
		else if(index == size) {
			findNode(index - 1).setNext(nuevo);
		}else {
		nuevo.setNext(findNode(index));
		findNode(index - 1).setNext(nuevo);
		}
		size++;
	}

	@Override
	public boolean remove(E obj) {
		Node<E> current = head.getNext();
		for(int i = 0; i < size(); i++) {
			if(current.getElement().equals(obj)) {
				if(current == head.getNext()) {
					head.setNext(current.getNext());
				} else { 
					findNode(i - 1).setNext(current.getNext());
				}
				size--;
				return true;
			}
			current = current.getNext();
		}
		return false;
	}

	@Override
	public boolean remove(int index) {
		if(index == 0 && size > 0) {
			head.getNext().setNext(head.getNext().getNext());
			return true;
		}
		findNode(index - 1).setNext(findNode(index).getNext());
		size--;
		return true;
	}

	@Override
	public int removeAll(E obj) {
		int counter = 0;
		while(contains(obj)) {
			remove(firstIndex(obj));
			counter++;
		}
		return counter;
	}

	@Override
	public E get(int index) {
		return findNode(index).getElement();
	}

	@Override
	public E set(int index, E obj) {
		E etr = findNode(index).getElement();
		findNode(index).setElement(obj);
		return etr;
	}

	@Override
	public E first() {
		if(size == 0) return null;
		return head.getNext().getElement();
	}

	@Override
	public E last() {
		return findNode(size - 1).getElement();
	}

	@Override
	public int firstIndex(E obj) {
		for(int i = 0; i < size(); i++) {
			if(findNode(i).getElement().equals(obj)) return i;
		}
		return -1;
	}

	@Override
	public int lastIndex(E obj) {
		for(int i = size - 1; i >=0; i--) {
			if(findNode(i).getElement().equals(obj)) return i;
		}
		return -1;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public boolean contains(E obj) {
		if(firstIndex(obj) >= 0) return true;
		return false;
	}

	@Override
	public void clear() {
		head.setNext(null);
	}
	
	private Node<E> findNode(int index) throws IndexOutOfBoundsException{
		if(this.isEmpty()) return null;
		if(index < 0 || index > size() - 1) throw new IndexOutOfBoundsException();
		 Node<E> current = head;
		 for(int i = 0; i <= index; i++) {
			 current = current.getNext();
		 }
		return current;
		
	}
	
	private static class Node<E> {
		private E element;
		private Node<E> next;
		
		public Node() {
			this.element = null;
			this.next = null;
		}
		
		public Node(E e, Node<E> n) {
			this.element = e;
			this.next = n;
		}
		
		public E getElement() {
			return element;
		}
		
		public void setElement(E e) {
			this.element = e;
		}
		
		public Node<E> getNext() {
			return this.next;
		}
		
		public void setNext(Node<E> e) {
			this.next = e;
		}
	}
}
