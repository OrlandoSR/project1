package edu.uprm.ece.icom4035.list;

import java.util.Iterator;

public class ArrayList<E> implements List<E>{
	
	public E[] elements;
	public int size;
	public final static int INITCAP = 5;
	
	public ArrayList(){
		elements = (E[]) new Object[INITCAP];
		size = 0;
	}

	@Override
	public Iterator<E> iterator() {
		ListIterator1<E> iter = new ListIterator1<E>(this);
		return iter;
	}

	@Override
	public void add(E obj) {
		this.add(this.size(), obj);
	}

	@Override
	public void add(int index, E obj) throws IndexOutOfBoundsException {
		if(index < 0 || index > size) throw new IndexOutOfBoundsException();
		if(currentCapacity() > size) {
			moveDataOnePositionTR(index, size - 1);
			elements[index] = obj;
		}else {
			changeCapacityTo(currentCapacity() + 1);
			moveDataOnePositionTR(index, size - 1);
			elements[index] = obj;
		}
		size++;
		changeCapacityTo(size);
	}

	@Override
	public boolean remove(E obj) {
		for(int i = 0; i < size - 1; i++) {
			if(elements[i].equals(obj)) {
				remove(i);
			}
		}
		return false;
	}

	@Override
	public boolean remove(int index) {
		if(index < 0 || index > size - 1) throw new IndexOutOfBoundsException();
		moveDataOnePositionTL(index + 1, size - 1);
		size--;
		return true;
	}

	@Override
	public int removeAll(E obj) {
		int counter = 0;
		while(contains(obj)) {
			remove(firstIndex(obj));
			counter++;
		}
		return counter;
	}

	@Override
	public E get(int index) {
		if(index < 0 || index > size - 1) throw new IndexOutOfBoundsException();
		return elements[index];
	}

	@Override
	public E set(int index, E obj) {
		if(index < 0 || index > size - 1) throw new IndexOutOfBoundsException();
		E temp = elements[index];
		elements[index] = obj;
		return temp;
	}

	@Override
	public E first() {
		if(isEmpty()) return null;
		return elements[0];
	}

	@Override
	public E last() {
		if(isEmpty()) return null;
		return elements[size - 1];
	}

	@Override
	public int firstIndex(E obj) {
		for(int i = 0; i < size; i++) {
			if(elements[i].equals(obj)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int lastIndex(E obj) {
		for(int i = size - 1; i >= 0; i--) {
			if(elements[i].equals(obj)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public boolean contains(E obj) {
		if(firstIndex(obj) >= 0) return true;
		return false;
	}

	@Override
	public void clear() {
			elements = null;
	}
	
	private int currentCapacity() {
		return elements.length;
	}
	
	private void changeCapacityTo(int newCapacity) { 
		E[] newElement = (E[]) new Object[newCapacity]; 
		for (int i=0; i<size; i++) { 
			newElement[i] = elements[i]; 
			elements[i] = null; 
		} 
		elements = newElement; 
	}
	
	private void moveDataOnePositionTR(int low, int sup) { 
		for (int pos = sup; pos >= low; pos--)
			elements[pos+1] = elements[pos]; 
	}
	
	private void moveDataOnePositionTL(int low, int sup) { 
		for (int pos = low; pos <= sup; pos++)
			elements[pos-1] = elements[pos]; 
	}
	
}
