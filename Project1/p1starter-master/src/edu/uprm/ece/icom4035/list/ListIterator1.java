package edu.uprm.ece.icom4035.list;

import java.util.Iterator;

public class ListIterator1<E> implements Iterator<E> {
	
	public int pos;
	public List<E> lst;
	public boolean canRemove;
	
	
	public ListIterator1(List <E> e) {
		this.pos = 0;
		this.lst = e;
		canRemove = false;
	}

	@Override
	public boolean hasNext() {
		return this.pos < lst.size();
	}

	@Override
	public E next() throws IllegalStateException{
		if(!this.hasNext()) throw new IllegalStateException();
		else {
		canRemove = true;
		pos++;
		return lst.get(pos - 1);
		}
	}
	
	@Override
	public void remove() {
		if(!canRemove) throw new IllegalStateException();
		lst.remove(--pos);
		canRemove = false;
	}

}
